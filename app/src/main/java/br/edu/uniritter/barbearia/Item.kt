package br.edu.uniritter.barbearia

data class Item (val titulo: String, val descricao: String, val preco: Int)