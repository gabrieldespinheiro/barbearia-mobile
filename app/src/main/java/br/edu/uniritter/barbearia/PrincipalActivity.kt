package br.edu.uniritter.barbearia

import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_principal.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.jar.Manifest

class PrincipalActivity : AppCompatActivity() {
    private val PERMISSION_CODE: Int = 1000
    private val IMAGE_CAPTURE_CODE: Int = 1001
    private val lastVisibleItemPosition: Int
        get() = linearLayoutManager.findLastVisibleItemPosition()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: ListAdapter
    private var servicos: ArrayList<Item> = ArrayList()
    private var photoPath: String = ""

    /*listOf(
        Item("Cabelo","Corte de cabelo", 20 ),
        Item("Barba","Corte de barba", 10 ),
        Item("Cabelo e barba","Corte de cabelo e barba", 30 )
    )*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        servicos.add(Item("Cabelo","Corte de cabelo", 20 ))
        servicos.add(Item("Barba","Corte de barba", 10 ))
        servicos.add(Item("Cabelo e barba","Corte de cabelo e barba", 30 ))

        adapter = ListAdapter(servicos)
        recyclerView.adapter = adapter
        setRecyclerViewScrollListener()
        setRecyclerViewItemTouchListener()


        btn_camera.setOnClickListener {
            Log.i("PrincipalActivity", "Quer abrir a câmera!")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    Log.e("PrincipalActivity", "Não conseguiu abrir a porra da câmera caraio!")
                    val permission  = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    requestPermissions(permission, PERMISSION_CODE)
                } else {
                    openCamera()
                    Log.w("PrincipalActivity", "Abriu a câmera cara!")
                }
            } else {
                openCamera()
                Log.w("PrincipalActivity", "Abriu a câmera cara!")
            }

        }

    }
    private fun setRecyclerViewScrollListener() {
        Log.i("PrincipalActivity", "Esse método não faz nada!")
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                // ADD servico
            }
        })
    }
    private fun setRecyclerViewItemTouchListener() {

        //1
        val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, viewHolder1: RecyclerView.ViewHolder): Boolean {
                //2
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                //3
                val position = viewHolder.adapterPosition
                servicos.removeAt(position)
                recyclerView.adapter!!.notifyItemRemoved(position)
            }
        }

        //4
        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    private fun openCamera() {
        Log.w("PrincipalActivity", "Abrindo câmera")

        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        val fileUri = contentResolver
            .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(packageManager) !== null) {
            photoPath = fileUri.toString()
            var file = createImageFile()
            if (file != null ) {
                var photoUri = FileProvider.getUriForFile(this,
                    "br.edu.uniritter.barbearia.fileprovider",
                    file)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, file)
                //galleryAddPic()
                startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
            }
        }

    }
    public fun saveImageToStorage() {
        val externalStorage = Environment.getExternalStorageState()
        if ( externalStorage.equals(Environment.MEDIA_MOUNTED) ) {
            val storageDirectory = Environment.getExternalStorageDirectory().toString()
            val file = File(storageDirectory, "test_image.jpg")
        } else {
            Toast.makeText(this, "Não foi possivel salvar a imagem", Toast.LENGTH_SHORT).show()
        }

    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        /*val storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES)*/
        val image = File.createTempFile(
            imageFileName, /* prefix */
            ".jpg", /* suffix */
            storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        photoPath = image.absolutePath
        return image
    }

    private fun galleryAddPic() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(photoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            sendBroadcast(mediaScanIntent)
        }
    }


}
