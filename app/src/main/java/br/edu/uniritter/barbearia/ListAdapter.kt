package br.edu.uniritter.barbearia

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.list_item.view.*

class ListAdapter(private val list: List<Item>) : RecyclerView.Adapter<ListAdapter.ItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAdapter.ItemViewHolder {
        Log.i("ListAdapter", "onCreateViewHolder do ListAdapter")
        val inflatedView = parent.inflate(R.layout.list_item, false)
        return ItemViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item: Item = list[position]
        holder.bind(item)
        Log.i("ListAdapter", "onBindViewHolder bindou item: " + item.descricao)
    }

    override fun getItemCount(): Int = list.size

    class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        //2
        private var view: View = v
        private var item: Item? = null

        //3
        init {
            v.setOnClickListener(this)
        }

        //4
        override fun onClick(v: View) {
            Log.i("ListAdapter", "clicou num botão")

        }

        fun bind(item: Item) {
            this.item = item
            view.list_title.text = item.titulo
            view.list_description.text = item.descricao
            view.list_price.text = item.preco.toString()

        }


        companion object {
            //5
            private val PHOTO_KEY = "PHOTO"
        }
    }

}