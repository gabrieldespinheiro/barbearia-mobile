package br.edu.uniritter.barbearia

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_principal.*

class MainFragment : Fragment() {

    private val servicos = listOf(
        Item("Cabelo","Corte de cabelo", 20 ),
        Item("Barba","Corte de barba", 10 ),
        Item("Cabelo e barba","Corte de cabelo e barba", 30 )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i("MainFragment", "OnCreate do MainFragment")
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.activity_principal, container, false)

    // populate the views now that the layout has been inflated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i("MainFragment", "onViewCreated do MainFragment")
        super.onViewCreated(view, savedInstanceState)
        // RecyclerView node initialized here
        recyclerView.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
            adapter = ListAdapter(servicos)
        }
    }

    companion object {
        fun newInstance(): MainFragment = MainFragment()
    }
}