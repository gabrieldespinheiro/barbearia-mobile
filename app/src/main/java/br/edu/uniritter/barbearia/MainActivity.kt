package br.edu.uniritter.barbearia

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_main.*
import br.edu.uniritter.barbearia.databinding.ActivityMainBinding
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i("MyActivity", "Entrou no MyActivity")
        super.onCreate(savedInstanceState)
        val binding : ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        //setContentView(R.layout.activity_main)
        val user = Usuario("Joao", "joao@gmail.com", "olamundo")
        binding.user = user

        btn.setOnClickListener {
            Log.i("MyActivity", "Clicou no botão!")
            val intent = Intent(this, DetalheActivity::class.java)
            startActivity(intent)
        }



    }
}
