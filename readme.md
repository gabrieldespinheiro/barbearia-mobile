
# Barbearia Mobile
Desenvolvimento para cadeira "Computação para Dispositivos Móveis" na UniRitter.
Feito por Daniel Gomes, Gabriel Pinheiro e Matheus Moura.
### Instalação
 - Configurar os dados de usuário no GIT
```sh
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```
 - Clonando o repositório
```sh
$ git clone https://gitlab.com/gabrieldespinheiro/barbearia-mobile.git
$ cd barbearia-mobile/
```
 - Criar branch para desenvolvimento
```sh
$ git checkout -b <nome_branch>
```
### Comandos básicos do GIT
 - Baixar a versão atualizada da repositorio
```sh
$ git pull origin <nome_branch*>
```
(*nome_branch) Geralmente damos pull do MASTER, que é a versão de produção

 - Verificar arquivos que sofreram alteração
```sh
$ git status
```

 - Adicionar arquivos 
```sh
$ git add <nome_arquivo ou . (para adicionar todos)>
```
 - Commitar arquivos adicionados
```sh
$ git commit -m "Mensagem do commit"
```
 - Enviar o(s) commit(s) para o servidor
```sh
$ git push origin <nome_branch>
```
 - Exemplo de Fluxo
```sh
$ git checkout -b fulano
$ git pull origin master
$ echo "Barbearia" >> readme.md
$ git add readme.md
$ git commit -m "Adicionando o readme"
$ gti push origin fulano
```
##### Outros comandos
 - Descartar modificações
```sh
$ git checkout <nome_arquivo>
```
 - Remover arquivo dos adicionados

```sh
$ git reset <nome_arquivo>
```